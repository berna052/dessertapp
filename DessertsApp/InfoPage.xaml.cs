﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace DessertsApp
{
    public partial class InfoPage : ContentPage
    {
        public InfoPage()
        {
            InitializeComponent();
        }

        public InfoPage(Dessert dessert)
        {
            InitializeComponent();
            BindingContext = dessert;
            Title = "Info";
        }

        public InfoPage(DessertModel dessert)
        {
            InitializeComponent();
            BindingContext = dessert;
            Title = "Info";
        }

        async void AddFavorite_Handle(object sender, System.EventArgs e)
        {
            await DisplayAlert("Added to Favorite","","Ok");

            var dessertPic = dessertPicture.Source.ToString();
            dessertPic = dessertPic.Remove(0, 6);

            var dessert = new DessertModel
            {
                Name = dessertName.Text,
                Description = dessertDescription.Text,
                Price = Int32.Parse(dessertPrice.Text),
                Picture = dessertPic
            };
            await App.Database.SaveItemAsync(dessert);
        }

        async void AddToCart_Handle(object sender, System.EventArgs e)
        {
            await DisplayAlert("Added to Cart", "", "Ok");

            var dessertPic = dessertPicture.Source.ToString();
            dessertPic = dessertPic.Remove(0, 6);

            var dessert = new DessertModel
            {
                Name = dessertName.Text,
                Description = dessertDescription.Text,
                Price = Int32.Parse(dessertPrice.Text),
                Picture = dessertPic
            };
            await App.CartDB.SaveItemAsync(dessert);
        }
    }
}
