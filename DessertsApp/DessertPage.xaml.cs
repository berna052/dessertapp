﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace DessertsApp
{
    public partial class DessertPage : ContentPage
    {
        ObservableCollection<Dessert> DessertCollection = new ObservableCollection<Dessert>();

        public DessertPage()
        {
            InitializeComponent();

            Title = "Desserts";

            PopulateList();
        }

        private void PopulateList()
        {
            var crepesCake = new Dessert
            {
                Name = "Crepes Cake",
                Description = "This is cake made of crepes with chocolate and fruit. You can choose between banana, strawberry, or pineapple",
                Price = 9,
                Picture = ImageSource.FromFile("crepesCake.png"),
            };

            var cheeseCake = new Dessert
            {
                Name = "Cheesecake",
                Description = "Our famous cheese cake that can be choosen between strawberry, oreo, or regular",
                Price = 15,
                Picture = ImageSource.FromFile("cheesecake.png"),
            };

            var carrotCake = new Dessert
            {
                Name = "Carrot Cake",
                Description = "A delicious cake of carrot covered in a cheese cream",
                Price = 20,
                Picture = ImageSource.FromFile("carrotCake.png")
            };

            var chocolateCake = new Dessert
            {
                Name = "Chocolate Cake",
                Description = "A cake for the chocolate lovers, this is a cake is made of pure chocholate",
                Price = 8,
                Picture = ImageSource.FromFile("chocolateCake.png")
            };

            var tresLechesCake = new Dessert
            {
                Name = "Tres Leches Cake",
                Description = "This cake is soaked in three kinds of milk: evaporated milk, condensed milk, and heavy cream",
                Price = 10,
                Picture = ImageSource.FromFile("tresLechesCake.png")
            };

            var cookiesChips = new Dessert
            {
                Name = "Chocolate Chip Cookies",
                Description = "Delicious chocolate chip cookies, they come in groups of 10 pieces",
                Price = 7,
                Picture = ImageSource.FromFile("chocolateChipCookies.png")
            };

            var cakePop = new Dessert
            {
                Name = "Cake Pops",
                Description = "You can choose between chocolate or vanilla. The cover can be chocolate or white chocolate. They come in groups of 5 pieces",
                Price = 10,
                Picture = ImageSource.FromFile("cakePop.png")
            };

            var tart = new Dessert
            {
                Name = "Tart",
                Description = "Delicious tart with a special cheese cream. You can choose any combination of strawberry, kiwi, banana, raspberry, blueberry, and blackberry. They come in groups of 5 pieces",
                Price = 10,
                Picture = ImageSource.FromFile("tart.png")
            };

            DessertCollection.Add(crepesCake);
            DessertCollection.Add(cheeseCake);
            DessertCollection.Add(carrotCake);
            DessertCollection.Add(chocolateCake);
            DessertCollection.Add(tresLechesCake);
            DessertCollection.Add(cookiesChips);
            DessertCollection.Add(cakePop);
            DessertCollection.Add(tart);
            DessertListView.ItemsSource = DessertCollection;
        }

        async void InfoPage_Tapped(object sender, System.EventArgs e)
        {
            Dessert dessert = (Dessert)DessertListView.SelectedItem;
            await Navigation.PushAsync(new InfoPage(dessert));
        }
    }
}
