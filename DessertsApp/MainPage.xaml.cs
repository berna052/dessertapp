﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DessertsApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void DessertButton_Handle(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new DessertPage());
        }

        async void FavoriteButton_Handle(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new FavoritesPage());
        }

        async void ShoppingCart_Handle(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new ShoppingPage());
        }

        async void ContactPage_Handle(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new ContactPage());
        }
    }
}
