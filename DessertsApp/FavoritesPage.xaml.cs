﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace DessertsApp
{
    public partial class FavoritesPage : ContentPage
    {
        public FavoritesPage()
        {
            InitializeComponent();

            Title = "Favorites";

            GetAllDesserts();
        }

        async void GetAllDesserts()
        {
            var allDesserts = await App.Database.GetAllItems();

            dessertList.ItemsSource = null;
            dessertList.ItemsSource = allDesserts;
        }

        void Handle_DeleteDessert(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var dessert = (DessertModel)menuItem.CommandParameter;
            App.Database.DeleteItem(dessert);
            GetAllDesserts();
        }

        async void InfoPage_Tapped(object sender, System.EventArgs e)
        {
            DessertModel dessert = (DessertModel)dessertList.SelectedItem;
            await Navigation.PushAsync(new InfoPage(dessert));
        }
    }
}
