﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;
using Xamarin.Forms;

namespace DessertsApp
{
    public class CartDatabase : ContentPage
    {
        readonly SQLiteAsyncConnection database;

        public CartDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<DessertModel>().Wait();
        }

        public Task<int> SaveItemAsync(DessertModel item)
        {
            if (item.ID != 0)
            {
                return database.UpdateAsync(item);
            }
            else
            {
                return database.InsertAsync(item);
            }
        }

        public Task<List<DessertModel>> GetAllItems()
        {
            return database.QueryAsync<DessertModel>("SELECT * FROM [DessertModel]");
        }

        public Task<List<DessertModel>> GetTotal()
        {
            var total = database.QueryAsync<DessertModel>("SELECT SUM(Price) FROM [DessertModel]");
            return total;
        }
    }
}

