﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace DessertsApp
{
    public partial class ShoppingPage : ContentPage
    {
        ObservableCollection<DessertModel> CartCollection = new ObservableCollection<DessertModel>();

        public ShoppingPage()
        {
            InitializeComponent();

            Title = "Cart";

            GetAllItems();

            GetTotalCost();
        }

        async void GetAllItems()
        {
            var allItems = await App.CartDB.GetAllItems();

            CartListView.ItemsSource = null;
            CartListView.ItemsSource = allItems;
        }

        async void GetTotalCost()
        {
            var totalCost = await App.CartDB.GetTotal();
            totalPrice.Text = totalCost.ToString();
        }
    }
}
