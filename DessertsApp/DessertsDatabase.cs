﻿using System;

using Xamarin.Forms;
using SQLite;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DessertsApp
{
    public class DessertsDatabase
    {
        readonly SQLiteAsyncConnection database;

        public DessertsDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<DessertModel>().Wait();
        }

        public Task<int> SaveItemAsync(DessertModel item)
        {
            if (item.ID != 0)
            {
                return database.UpdateAsync(item);
            }
            else
            {
                return database.InsertAsync(item);
            }
        }

        public Task<List<DessertModel>> GetAllItems()
        {
            return database.QueryAsync<DessertModel>("SELECT * FROM [DessertModel]");
        }

        public Task<int> DeleteItem(DessertModel dessert)
        {
            return database.DeleteAsync(dessert);
        }
    }
}

