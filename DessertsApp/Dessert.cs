﻿using System;

using Xamarin.Forms;

namespace DessertsApp
{
    public class Dessert
    {
        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public int Price
        {
            get;
            set;
        }

        public object Picture
        {
            get;
            set;
        }
    }
}

