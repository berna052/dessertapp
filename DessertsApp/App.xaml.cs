﻿using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace DessertsApp
{
    public partial class App : Application
    {
        public App ()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }

        static DessertsDatabase database;
        public static DessertsDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new DessertsDatabase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DessertsSQLite.db3"));
                }
                return database;
            }
        }

        static CartDatabase cartDB;
        public static CartDatabase CartDB
        {
            get
            {
                if (cartDB == null)
                {
                    cartDB = new CartDatabase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "CartSQLite.db3"));
                }
                return cartDB;
            }
        }

        protected override void OnStart ()
        {
            // Handle when your app starts
        }

        protected override void OnSleep ()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume ()
        {
            // Handle when your app resumes
        }
    }
}
