﻿using System;
using SQLite;
using Xamarin.Forms;

namespace DessertsApp
{
    public class DessertModel
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public string Picture { get; set; }
    }
}

